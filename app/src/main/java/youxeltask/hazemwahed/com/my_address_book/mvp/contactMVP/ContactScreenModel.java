package youxeltask.hazemwahed.com.my_address_book.mvp.contactMVP;

import android.content.Context;
import android.util.Log;

import java.util.List;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.data.DatabaseManager;
import youxeltask.hazemwahed.com.my_address_book.data.repository.ContactRepository;
import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract;
import youxeltask.hazemwahed.com.my_address_book.data.repository.QueryById;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.utility.schedulers.SchedulerProvider;

/**
 * Created by Hazem on 10/7/2017.
 */

public class ContactScreenModel implements ContacScreenMVP.ContactModel {

    private Context context;
    private DatabaseManager dbManager;
    private ContactRepository contactRepository;
    private SchedulerProvider schedulerProvider;


    public ContactScreenModel(Context context) {
        this.context = context;
        schedulerProvider=SchedulerProvider.getInstance();
        dbManager=DatabaseManager.getDatabaseManager(context,schedulerProvider);
        contactRepository=new ContactRepository(dbManager);
    }

    @Override
    public Observable<List<UserContact>> getContactList(String userId) {
        QueryById queryById=new QueryById(DatabaseContract.ContactTable.TABLE_NAME,userId, DatabaseContract.ContactTable.CONTACT_USER_ID);
        queryById.setOrder("COLLATE NOCASE ASC",DatabaseContract.ContactTable.CONTACT_NAME_COL);
        return contactRepository.query(queryById)
                .observeOn(schedulerProvider.ui());
    }

    @Override
    public void deleteContact(UserContact userContact) {
        boolean v=contactRepository.remove(userContact);
        Log.d("Contact Screen",v+"");
    }
}
