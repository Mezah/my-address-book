package youxeltask.hazemwahed.com.my_address_book.mvp.contactMVP;

import android.util.Log;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;

/**
 * Created by Hazem on 10/7/2017.
 */

public class ContactScreenPresenter implements ContacScreenMVP.ContactPresenter {

    private static final String TAG = "ContactPresenter";
    private ContacScreenMVP.ContactView contactView;
    private ContacScreenMVP.ContactModel contactModel;
    private CompositeDisposable disposables;

    public ContactScreenPresenter(ContacScreenMVP.ContactModel contactModel) {
        this.contactModel = contactModel;
        disposables = new CompositeDisposable();
    }

    @Override
    public void addView(ContacScreenMVP.ContactView contactView) {
        this.contactView = contactView;
    }

    @Override
    public void removeView() {
        if (disposables != null)
            disposables.dispose();
        contactView = null;
    }

    @Override
    public boolean isViewAdded() {
        return contactView != null;
    }

    @Override
    public void getContactList(String userId) {
        contactModel.getContactList(userId)
                .subscribe(new Observer<List<UserContact>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposables.add(d);
                    }
                    @Override
                    public void onNext(@NonNull List<UserContact> userContacts) {
                        if (Utility.checkContactList(userContacts))
                            contactView.showContactList(userContacts);
                        else
                            contactView.showEmptyContactList();
                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d(TAG, "onError: " + e.getLocalizedMessage());
                    }
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: stream completed");
                    }
                });
    }

    @Override
    public void deleteContact(UserContact userContact) {
        contactModel.deleteContact(userContact);
    }
}
