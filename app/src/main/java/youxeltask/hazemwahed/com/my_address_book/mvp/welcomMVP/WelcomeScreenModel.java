package youxeltask.hazemwahed.com.my_address_book.mvp.welcomMVP;

import android.content.Context;
import android.util.Log;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.data.DatabaseManager;
import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract;
import youxeltask.hazemwahed.com.my_address_book.data.repository.QueryWithoutId;
import youxeltask.hazemwahed.com.my_address_book.data.repository.UserRepository;
import youxeltask.hazemwahed.com.my_address_book.model.User;
import youxeltask.hazemwahed.com.my_address_book.utility.schedulers.SchedulerProvider;

/**
 * Created by Hazem on 10/7/2017.
 */

public class WelcomeScreenModel implements WelcomScreenMVP.WelcomeModel {

    private UserRepository userRepository;
    private DatabaseManager databaseManager;
    private SchedulerProvider schedulerProvider;
    private QueryWithoutId queryById;

    public WelcomeScreenModel(Context context) {
        schedulerProvider=SchedulerProvider.getInstance();
        databaseManager=DatabaseManager.getDatabaseManager(context,schedulerProvider);
        userRepository=new UserRepository(databaseManager);
    }

    @Override
    public Observable<User> getUserData() {
        queryById = new QueryWithoutId(DatabaseContract.UserTable.TABLE_NAME);
        return userRepository.singleItemQuery(queryById)
                .observeOn(schedulerProvider.ui());
    }

    @Override
    public boolean isUserSaved() {
        return false;
    }

    @Override
    public void saveUserValue(User user) {
       userRepository.add(user);
    }

    @Override
    public void updateUser(User user) {
        boolean b=userRepository.update(user);
        Log.d("ED",b+"");
    }
}
