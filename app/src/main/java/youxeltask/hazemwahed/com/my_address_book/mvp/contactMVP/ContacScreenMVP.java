package youxeltask.hazemwahed.com.my_address_book.mvp.contactMVP;

import java.util.List;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;

/**
 * Created by Hazem on 10/7/2017.
 */

public interface ContacScreenMVP {

    interface ContactModel{
        Observable<List<UserContact>> getContactList(String userId);

        void deleteContact(UserContact userContact);
    }

    interface ContactView{
        void showContactList(List<UserContact> contactList);
        void showEmptyContactList();
        void hideEmptyContactList();
        void updateContactList();
        void createDummyData(List<UserContact> contactList);
    }

    interface ContactPresenter{

        void addView(ContactView contactView);
        void removeView();
        boolean isViewAdded();
        void getContactList(String userId);

        void deleteContact(UserContact userContact);
    }
}
