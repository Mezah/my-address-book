package youxeltask.hazemwahed.com.my_address_book.mvp.addcontactMVP;

import android.content.Context;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.data.DatabaseManager;
import youxeltask.hazemwahed.com.my_address_book.data.repository.ContactRepository;
import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract;
import youxeltask.hazemwahed.com.my_address_book.data.repository.QueryById;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.utility.schedulers.SchedulerProvider;

/**
 * Created by Hazem on 10/7/2017.
 */

public class AddContactScreenModel implements AddContactMVP.AddContactModel {

    private Context context;
    private SchedulerProvider schedulerProvider;
    private DatabaseManager databaseManager;
    private ContactRepository contactRepository;

    public AddContactScreenModel(Context context) {
        this.context = context;
        schedulerProvider = SchedulerProvider.getInstance();
        databaseManager = DatabaseManager.getDatabaseManager(context, schedulerProvider);
        contactRepository = new ContactRepository(databaseManager);
    }

    @Override
    public void saveContactInformation(UserContact contact) {
        contactRepository.add(contact);
    }

    @Override
    public Observable<UserContact> getContactFromDatabase(String contactName) {
        QueryById queryById = new QueryById(DatabaseContract.ContactTable.TABLE_NAME, contactName, DatabaseContract.ContactTable.CONTACT_USER_ID);
        return contactRepository.singleItemQuery(queryById)
                .observeOn(schedulerProvider.ui());
    }
}
