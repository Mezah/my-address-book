package youxeltask.hazemwahed.com.my_address_book.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract;

/**
 * Created by Hazem on 10/6/2017.
 *
 */

public class DBHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "ADDRESS_APP";
    private static final int DATABASE_VERSION = 2;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DatabaseContract.UserTable.CREATE_STATEMENT);
        sqLiteDatabase.execSQL(DatabaseContract.ContactTable.CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DatabaseContract.UserTable.CREATE_STATEMENT);
    }
}
