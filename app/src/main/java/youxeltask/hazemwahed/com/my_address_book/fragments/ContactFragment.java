package youxeltask.hazemwahed.com.my_address_book.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.LandingAnimator;
import youxeltask.hazemwahed.com.my_address_book.R;
import youxeltask.hazemwahed.com.my_address_book.activities.ContactActivity;
import youxeltask.hazemwahed.com.my_address_book.activities.SearchActivity;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.mvp.contactMVP.ContacScreenMVP;
import youxeltask.hazemwahed.com.my_address_book.mvp.contactMVP.ContactScreenModel;
import youxeltask.hazemwahed.com.my_address_book.mvp.contactMVP.ContactScreenPresenter;

/**
 * Created by Hazem on 10/7/2017.
 */

public class ContactFragment extends Fragment implements ContacScreenMVP.ContactView, View.OnClickListener {

    public static final String FRAGMENT_TAG = "contact_tag";
    public static final String CONTACT_LIST = "contact_list";
    public static final int ANIMTAION_TIME = 300;
    private FloatingActionButton addContact;
    private TextView emptyView;
    private ImageView searchView, deleteContact;
    private RecyclerView contactRecycler;
    private ContactAdapter contactAdapter;
    private ContacScreenMVP.ContactPresenter contactPresenter;
    private ContacScreenMVP.ContactModel contactModel;
    private AddContactViewer addContactViewer;
    private String userId;
    private List<UserContact> dummylist;


    public static ContactFragment getContactFragment(Bundle bundle) {
        ContactFragment contactFragment = new ContactFragment();
        contactFragment.setArguments(bundle);
        return contactFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        addContactViewer = (AddContactViewer) getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactAdapter = new ContactAdapter();
        contactAdapter.setContactList(new ArrayList<>());
        contactModel = new ContactScreenModel(getActivity());
        contactPresenter = new ContactScreenPresenter(contactModel);
        contactPresenter.addView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        addContact = view.findViewById(R.id.add_contact);
        emptyView = view.findViewById(R.id.empty_view);
        searchView = view.findViewById(R.id.search_icon);
        contactRecycler = view.findViewById(R.id.contact_recycler);
        contactRecycler.setItemAnimator(new LandingAnimator());
        contactRecycler.getItemAnimator().setRemoveDuration(ANIMTAION_TIME);
        contactRecycler.getItemAnimator().setChangeDuration(ANIMTAION_TIME);
        contactRecycler.getItemAnimator().setMoveDuration(ANIMTAION_TIME);
        contactRecycler.getItemAnimator().setAddDuration(ANIMTAION_TIME);


        addContact.setOnClickListener(this);
        searchView.setOnClickListener(this);
        //prepare recycler
        contactRecycler.setAdapter(contactAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        contactRecycler.setLayoutManager(layoutManager);
        Bundle bundle = getArguments();
        if (bundle != null) {
            userId = bundle.getString(ContactActivity.USER_ID);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contactPresenter.getContactList(userId);
    }

    @Override
    public void onDestroy() {
        contactPresenter.removeView();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        addContactViewer = null;
    }


    @Override
    public void onClick(View view) {
        final int id = view.getId();

        switch (id) {
            case R.id.add_contact: {
                addContactViewer.showNewAddContactScreen(userId);
                break;
            }
            case R.id.search_icon: {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                intent.putParcelableArrayListExtra(CONTACT_LIST, (ArrayList<? extends Parcelable>) contactAdapter.getContactList());
                intent.putExtra(ContactActivity.USER_ID, userId);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anime_search_in, 0);

            }
        }
    }

    @Override
    public void showContactList(List<UserContact> contactList) {
        contactAdapter.setContactList(contactList);
    }

    @Override
    public void showEmptyContactList() {

        if (dummylist != null && !dummylist.isEmpty()) {
            if (emptyView.getVisibility() == View.VISIBLE) {
                emptyView.setVisibility(View.GONE);
            }
        }
        else
            emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyContactList() {
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void updateContactList() {
        contactPresenter.getContactList(userId);
    }

    @Override
    public void createDummyData(List<UserContact> contactList) {
        dummylist = contactList;
        hideEmptyContactList();
        contactAdapter.setContactList(contactList);

    }


    //region adapter
    private final class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {

        private List<UserContact> contactList;


        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item_list, parent, false);
            ContactViewHolder viewHolder = new ContactViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ContactViewHolder holder, int position) {
            UserContact contact = contactList.get(position);
            holder.setContact(contact);
        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }

        public void setContactList(List<UserContact> contactList) {
            if (this.contactList != null)
                if (!this.contactList.isEmpty())
                    this.contactList.clear();
            this.contactList = contactList;
            notifyDataSetChanged();
        }

        public List<UserContact> getContactList() {
            return contactList;
        }

        public void deleteItem(int position) {
            contactList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private final class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView photo, deleteContact;
        private final TextView name, phone;
        private UserContact userContact;
        private View container;
        private int contactPostion;

        public ContactViewHolder(View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.list_contact_photo);
            name = itemView.findViewById(R.id.list_contact_name);
            phone = itemView.findViewById(R.id.list_contact_phone);
            container = itemView.findViewById(R.id.list_container);
            deleteContact = itemView.findViewById(R.id.list_delete_contact);
            photo.setOnClickListener(this);
            deleteContact.setOnClickListener(this);

        }

        public void setContact(UserContact contact) {
            this.userContact = contact;
            name.setText(contact.getContactName());
            phone.setText(contact.getContactPhone());
            if (contact.getContactPhoto() != null)
                Glide.with(getActivity()).load(contact.getContactPhoto()).into(photo);
        }

        @Override
        public void onClick(View v) {
            final int id = v.getId();
            switch (id) {
                case R.id.list_container: {
                    addContactViewer.showEditContact(userContact, userId, 0);
                    break;
                }
                case R.id.list_contact_photo: {
                    addContactViewer.showEditContact(userContact, userId, 0);
                    break;
                }
                case R.id.list_delete_contact: {
                    contactAdapter.deleteItem(getAdapterPosition());
                    contactPresenter.deleteContact(userContact);
                    break;
                }


            }
        }
    }
    //endregion

    //region interfaces
    public interface AddContactViewer {
        void showNewAddContactScreen(String userId);

        void showEditContact(UserContact contact, String userID, int contactPosition);
    }
    //endregion
}
