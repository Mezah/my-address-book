package youxeltask.hazemwahed.com.my_address_book.mvp.addcontactMVP;

import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;

import static youxeltask.hazemwahed.com.my_address_book.utility.Utility.entryNotValid;

/**
 * Created by Hazem on 10/7/2017.
 */

public class AddContactScreenPresenter implements AddContactMVP.AddContactPresenter {

    private static final String TAG = "AddContactPresenter";
    private AddContactMVP.AddContactView addContactView;
    private AddContactMVP.AddContactModel addContactModel;
    private CompositeDisposable disposable;

    public AddContactScreenPresenter(AddContactMVP.AddContactModel addContactModel) {
        this.addContactModel = addContactModel;
        disposable = new CompositeDisposable();
    }

    @Override
    public void addView(AddContactMVP.AddContactView addContactView) {
        this.addContactView = addContactView;
        disposable = new CompositeDisposable();
    }


    @Override
    public void removeView() {
        if (disposable != null)
            disposable.dispose();
        addContactView = null;
    }

    @Override
    public boolean isViewAdded() {
        return addContactView != null;
    }

    @Override
    public void getContactInformation(String userId, String contactName, String contactPhone) {
        addContactModel.getContactFromDatabase(userId)
                .subscribe(new Observer<UserContact>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(@NonNull UserContact userContacts) {
                        if (!userContacts.getContactName().equals(Utility.NO_DATA))
                            addContactView.showContactInformation(userContacts);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d(TAG, "onError: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: stream complete");
                    }
                });
    }

    @Override
    public void saveContactInformation(String userID) {
        String contactName = addContactView.getContactName();
        String contactPhone = addContactView.getContactPhone();
        String contactEmail = addContactView.getContactEmail();
        String contactNicName = addContactView.getContactNickName();
        String contactGender = addContactView.getUserGender();
        String contactPhoto = addContactView.getPhotoUrl();

        if (entryNotValid(contactName) || entryNotValid(contactPhone)) {
            addContactView.showErrorMessage();
            addContactView.showContacetMissingDataAnimation();
        } else {
            UserContact userContact = new UserContact();
            userContact.setContactName(contactName);
            userContact.setContactPhone(contactPhone);

            if (!entryNotValid(contactEmail))
                userContact.setContactEmail(contactEmail);
            if (!entryNotValid(contactNicName))
                userContact.setContactNickName(contactNicName);
            if (!entryNotValid(contactPhoto))
                userContact.setContactPhoto(contactPhoto);

            userContact.setContactgender(contactGender);
            userContact.setUserId(userID);

            addContactModel.saveContactInformation(userContact);
            addContactView.showContactAddedButtonAnimation();

        }
    }
}
