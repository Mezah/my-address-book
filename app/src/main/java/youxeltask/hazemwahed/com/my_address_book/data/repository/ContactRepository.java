package youxeltask.hazemwahed.com.my_address_book.data.repository;

import android.content.ContentValues;

import java.util.List;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.data.DatabaseManager;
import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract.ContactTable;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;

/**
 * Created by Hazem on 10/7/2017.
 */

public class ContactRepository implements BaseRepository<UserContact> {


    private DatabaseManager dbManager;
    private final Utility.ContactToContentValues userContactToContvalues;
    private final Utility.CursorToUserContact cursorToUserContact;

    public ContactRepository(DatabaseManager dbManager) {
        this.dbManager = dbManager;
        userContactToContvalues = new Utility.ContactToContentValues();
        cursorToUserContact = new Utility.CursorToUserContact();
    }

    @Override
    public void add(UserContact userContact) {
        final ContentValues values = userContactToContvalues.map(userContact);
        dbManager.insert(ContactTable.TABLE_NAME, values);
    }

    @Override
    public void add(List<UserContact> userContacts) {
        for (UserContact userContact : userContacts) {
            final ContentValues values = userContactToContvalues.map(userContact);
            dbManager.insert(ContactTable.TABLE_NAME, values);
        }
    }

    @Override
    public boolean update(UserContact item) {
        final ContentValues values = userContactToContvalues.map(item);
        final String where = ContactTable.CONTACT_NAME_COL + "=?";
        final String[] args = {item.getContactName()};
        return dbManager.update(ContactTable.TABLE_NAME, values, where, args);
    }

    @Override
    public boolean remove(UserContact item) {
        final String where = ContactTable.CONTACT_NAME_COL + "=?";
        final String[] args = {item.getContactName()};
        return dbManager.delete(ContactTable.TABLE_NAME,  where, args);
    }

    @Override
    public Observable<UserContact> singleItemQuery(SQLStatement query) {
        return dbManager.query(ContactTable.TABLE_NAME, query.sqlStatement())
                .map(contactQuery -> Utility.getUserContact(contactQuery,cursorToUserContact));
    }

    @Override
    public Observable<List<UserContact>> query(SQLStatement query) {
        return dbManager.query(ContactTable.TABLE_NAME, query.sqlStatement())
                .map(contactQuery -> Utility.getUserContactList(contactQuery,cursorToUserContact));
    }
}
