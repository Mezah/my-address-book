package youxeltask.hazemwahed.com.my_address_book.utility.schedulers;

import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Allow providing different types of {@link Schedulers}s.
 * <p>
 * This help in testing where it may be needed to provide different type of threads that
 * the one in production code.
 */
public interface BaseSchedulerProvider {

    @NonNull
    Scheduler computation();

    @NonNull
    Scheduler io();

    @NonNull
    Scheduler ui();

    @NonNull
    Scheduler trampoline();
}
