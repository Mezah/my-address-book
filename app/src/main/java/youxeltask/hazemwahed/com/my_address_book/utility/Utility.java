package youxeltask.hazemwahed.com.my_address_book.utility;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.squareup.sqlbrite2.SqlBrite;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract.ContactTable;
import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract.UserTable;
import youxeltask.hazemwahed.com.my_address_book.data.repository.Mapper;
import youxeltask.hazemwahed.com.my_address_book.model.User;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;

/**
 * Created by Hazem on 10/6/2017.
 */

public class Utility {

    public static final String NO_DATA = "NO_DATA";


    /**
     * Add fragment to the view shown.
     *
     * @param fragment      The Fragment to be Added.
     * @param fragmentTag   Tag for the fragment to retrieve it later.
     * @param addToBacStack Either to add the Fragment to back stack or not.
     * @param withAnimation Allow or prevent fragment transition with animation.
     */
    public static void addFragment(AppCompatActivity activity, Fragment fragment, int fragmentContainer, String fragmentTag, boolean addToBacStack, boolean withAnimation) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (fragmentManager.findFragmentByTag(fragmentTag) == null) {
            // the fragment is not added before so add it and commit
            fragmentTransaction.replace(fragmentContainer, fragment, fragmentTag);
            if (addToBacStack)
                fragmentTransaction.addToBackStack(fragmentTag);
        } else {
            fragmentTransaction.replace(fragmentContainer, fragment, fragmentTag);
        }
        fragmentTransaction.commit();

    }

    public static final List<User> getUserList(SqlBrite.Query query, CursorToUser cursorToUser) {
        Cursor cursor = query.run();
        List<User> userList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                User user = cursorToUser.map(cursor);
                userList.add(user);
                cursor.moveToNext();
            }
            return userList;
        }
        return Collections.singletonList(getEmptyUser()); //return a list with single non null user
    }

    public static final User getUser(SqlBrite.Query query, CursorToUser cursorToUser) {
        Cursor cursor = query.run();
        if (cursor != null && cursor.moveToFirst())
            return cursorToUser.map(cursor);
        return Utility.getEmptyUser(); // return no null user
    }

    public static final UserContact getUserContact(SqlBrite.Query query, CursorToUserContact cursorToUser) {
        Cursor cursor = query.run();
        if (cursor != null && cursor.moveToFirst())
            return cursorToUser.map(cursor);
        return Utility.getEmptyUserContact(); // return no null user  contact
    }

    public static final List<UserContact> getUserContactList(SqlBrite.Query query, CursorToUserContact cursorToUserContact) {
        Cursor cursor = query.run();
        List<UserContact> userList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                UserContact userContact = cursorToUserContact.map(cursor);
                userList.add(userContact);
                cursor.moveToNext();
            }
            return userList;
        }
        return Collections.singletonList(getEmptyUserContact()); //return a list with single non null user contact
    }

    public static boolean checkContactList(List<UserContact> userContacts) {
        if (userContacts != null && userContacts.size() == 1) {
            // make sure the contact is a valid contact since a one empty contact is returned if not contacts found
            return !userContacts.get(0).getContactName().equals(NO_DATA);

        } else if (userContacts != null && userContacts.size() > 1) //check if the list contains valid data
            return true;

        return false;   // the data is not valid.
    }

    public static final User getEmptyUser() {
        User user = new User();
        user.createUserId();
        user.setPhotoUrl(NO_DATA);
        user.setUserName(NO_DATA);
        return user;
    }

    public static final UserContact getEmptyUserContact() {
        UserContact userContact = new UserContact();
        userContact.setUserId(NO_DATA);
        userContact.setContactName(NO_DATA);
        userContact.setContactgender(NO_DATA);
        userContact.setContactNickName(NO_DATA);
        userContact.setUserId(NO_DATA);
        userContact.setContactEmail(NO_DATA);
        userContact.setContactPhone(NO_DATA);
        userContact.setContactPhoto(NO_DATA);
        return userContact;
    }

    public static List<UserContact> createUserContactDummyList() {
        ArrayList<UserContact> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 40; i++) {
            UserContact userContact = new UserContact();
            userContact.setContactName("Contact " + i);
            userContact.setContactPhone(random.nextInt(12365)+6532 + "");
            userContact.setContactNickName("Contact Mick" + i);
            userContact.setContactEmail("Contact_" + i + "@mail.com");
            if (random.nextBoolean())
                userContact.setContactgender("Male");
            else
                userContact.setContactgender("Female");
            list.add(userContact);
        }

        return list;
    }

    public static final User getTestUser() {
        User user = new User();
        user.setUserId("1");
        user.setPhotoUrl("");
        user.setUserName("Hazem Waheed");
        return user;
    }

    public static boolean entryNotValid(String entry) {
        return entry == null || entry.isEmpty();
    }

    //region camera helper thanks to https://www.journaldev.com/13270/android-capture-image-camera-gallery

    /**
     * Create a chooser intent to select the source to get image from.
     * <p>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).
     * <p>
     * All possible sources are added to the intent chooser.
     */
    public static Intent getPickImageChooserIntent(Activity activity) {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri(activity);

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = activity.getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent(Activity)} ()}.
     * <p>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public static Uri getPickImageResultUri(Activity activity, Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }


        return isCamera ? getCaptureImageOutputUri(activity) : data.getData();
    }


    /**
     * Get URI to image received from capture by camera.
     */
    private static Uri getCaptureImageOutputUri(Activity activity) {
        Uri outputFileUri = null;
        File getImage = activity.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    //endregion

    public static final class UserToContentValues implements Mapper<User, ContentValues> {
        private final ContentValues values = new ContentValues();

        @Override
        public ContentValues map(User user) {
            values.put(UserTable.USER_ID_COL, user.getUserId());

            values.put(UserTable.USER_NAME, user.getUserName());

            values.put(UserTable.USER_PHOTO, user.getPhotoUrl());

            return values;
        }
    }

    public static final class CursorToUser implements Mapper<Cursor, User> {

        @Override
        public User map(Cursor cursor) {
            User user = new User();
            user.setUserId(cursor.getString(cursor.getColumnIndex(UserTable.USER_ID_COL)));
            user.setUserName(cursor.getString(cursor.getColumnIndex(UserTable.USER_NAME)));
            user.setPhotoUrl(cursor.getString(cursor.getColumnIndex(UserTable.USER_PHOTO)));
            return user;
        }
    }


    public static final class ContactToContentValues implements Mapper<UserContact, ContentValues> {
        private final ContentValues values = new ContentValues();

        @Override
        public ContentValues map(UserContact userContact) {
            values.put(ContactTable.CONTACT_NAME_COL, userContact.getContactName());
            values.put(ContactTable.CONTACT_PHONE_NUMBER, userContact.getContactPhone());
            values.put(ContactTable.CONTACT_EMAIL, userContact.getContactEmail());
            values.put(ContactTable.CONTACT_NICK_NAME, userContact.getContactNickName());
            values.put(ContactTable.CONTACT_PHOTO, userContact.getContactPhoto());
            values.put(ContactTable.CONTACT_GENDER, userContact.getContactgender());
            values.put(ContactTable.CONTACT_USER_ID, userContact.getUserId());

            return values;
        }
    }

    public static final class CursorToUserContact implements Mapper<Cursor, UserContact> {

        @Override
        public UserContact map(Cursor cursor) {
            UserContact userContact = new UserContact();
            userContact.setContactName(cursor.getString(cursor.getColumnIndex(ContactTable.CONTACT_NAME_COL)));
            userContact.setContactPhone(cursor.getString(cursor.getColumnIndex(ContactTable.CONTACT_PHONE_NUMBER)));
            userContact.setContactEmail(cursor.getString(cursor.getColumnIndex(ContactTable.CONTACT_EMAIL)));
            userContact.setContactPhoto(cursor.getString(cursor.getColumnIndex(ContactTable.CONTACT_PHOTO)));
            userContact.setContactNickName(cursor.getString(cursor.getColumnIndex(ContactTable.CONTACT_NICK_NAME)));
            userContact.setContactgender(cursor.getString(cursor.getColumnIndex(ContactTable.CONTACT_GENDER)));
            userContact.setUserId(cursor.getString(cursor.getColumnIndex(ContactTable.CONTACT_USER_ID)));
            return userContact;
        }
    }
}
