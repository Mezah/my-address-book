package youxeltask.hazemwahed.com.my_address_book.model;

import java.util.UUID;

/**
 * Created by Hazem on 10/6/2017.
 */

public class User {

    private UUID id;
    private String userId;
    private String userName;
    private String photoUrl;

    public User() {

    }

    public void createUserId() {
        id = UUID.randomUUID();

        setUserId(id.toString().replace("-", ""));
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }


}
