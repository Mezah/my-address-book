package youxeltask.hazemwahed.com.my_address_book.data.repository;

import android.content.ContentValues;

import java.util.List;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.data.DatabaseManager;
import youxeltask.hazemwahed.com.my_address_book.data.repository.DatabaseContract.UserTable;
import youxeltask.hazemwahed.com.my_address_book.model.User;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility.CursorToUser;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility.UserToContentValues;

/**
 * Created by Hazem on 10/6/2017.
 * <p>
 * This class implements {@link BaseRepository} to show how to persist user data in local database.
 */

public class UserRepository implements BaseRepository<User> {

    private DatabaseManager dbManager;
    private final UserToContentValues userToContvalues;
    private final CursorToUser cursorToUser;

    public UserRepository(DatabaseManager dbManager) {
        this.dbManager = dbManager;
        userToContvalues = new UserToContentValues();
        cursorToUser = new CursorToUser();
    }

    @Override
    public void add(User item) {
        final ContentValues values = userToContvalues.map(item);
        dbManager.insert(UserTable.TABLE_NAME, values);
    }

    @Override
    public void add(List<User> items) {
        for (User User : items) {
            final ContentValues values = userToContvalues.map(User);
            dbManager.insert(UserTable.TABLE_NAME, values);
        }
    }

    @Override
    public boolean update(User item) {
        final ContentValues values = userToContvalues.map(item);
        final String where = UserTable.USER_ID_COL + "=?";
        final String[] args = {item.getUserId()};
        return dbManager.update(UserTable.TABLE_NAME, values, where, args);
    }

    @Override
    public boolean remove(User item) {
        return dbManager.delete(UserTable.TABLE_NAME, "");
    }

    @Override
    public Observable<List<User>> query(SQLStatement query) {
        return dbManager.query(UserTable.TABLE_NAME, query.sqlStatement())
                .map(listQuery -> Utility.getUserList(listQuery, cursorToUser));
    }

    @Override
    public Observable<User> singleItemQuery(SQLStatement query) {
        return dbManager.query(UserTable.TABLE_NAME, query.sqlStatement())
                .map(userQuery -> Utility.getUser(userQuery, cursorToUser));
    }
}
