package youxeltask.hazemwahed.com.my_address_book.data.repository;

/**
 * Created by Hazem on 10/7/2017.
 */

public class QueryWithoutId implements SQLStatement {

    private String tableName;

    public QueryWithoutId(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String sqlStatement() {
        return "SELECT * FROM "+tableName;
    }
}
