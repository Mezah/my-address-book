package youxeltask.hazemwahed.com.my_address_book.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;

import youxeltask.hazemwahed.com.my_address_book.R;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.mvp.addcontactMVP.AddContactMVP;
import youxeltask.hazemwahed.com.my_address_book.mvp.addcontactMVP.AddContactScreenModel;
import youxeltask.hazemwahed.com.my_address_book.mvp.addcontactMVP.AddContactScreenPresenter;
import youxeltask.hazemwahed.com.my_address_book.utility.BounceInterpolator;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;

import static youxeltask.hazemwahed.com.my_address_book.activities.ContactActivity.CONTACT;
import static youxeltask.hazemwahed.com.my_address_book.activities.ContactActivity.USER_ID;
import static youxeltask.hazemwahed.com.my_address_book.utility.Utility.getPickImageResultUri;

/**
 * Created by Hazem on 10/7/2017.
 */

public class AddContactActivity extends AppCompatActivity implements AddContactMVP.AddContactView, View.OnClickListener {

    private static final int CAMERA_ACTION = 200;

    private ImageButton saveUserdata, cancel;
    private ImageView contactPhoto, choosePhoto;
    private Animation myAnim;
    private Animation myAnim2;
    private EditText contactName, contactPhone, contackNickName, contactMail;
    private AddContactMVP.AddContactPresenter addContactPresenter;
    private AddContactMVP.AddContactModel addContactModel;
    private String userID;
    private String photUrl;
    private RadioButton maleButton,femaleButton;

    //region lifecycle
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        getSupportActionBar().setTitle(R.string.add_contact);

        myAnim = AnimationUtils.loadAnimation(this, R.anim.button_scale);
        myAnim2 = AnimationUtils.loadAnimation(this, R.anim.button_shake);
        BounceInterpolator interpolator = new BounceInterpolator(0.2d, 20d);
        myAnim.setInterpolator(interpolator);

        contactName = findViewById(R.id.contact_name);
        contactPhone = findViewById(R.id.contact_phone);
        contackNickName = findViewById(R.id.contact_nick_name);
        contactMail = findViewById(R.id.contact_mail);
        contactPhoto = findViewById(R.id.contact_photo);
        choosePhoto = findViewById(R.id.contact_choose_photo);
        saveUserdata = findViewById(R.id.ok_bt);
        cancel = findViewById(R.id.canel_bt);
        maleButton=findViewById(R.id.male_button);
        femaleButton=findViewById(R.id.female_button);

        saveUserdata.setOnClickListener(this);
        cancel.setOnClickListener(this);
        choosePhoto.setOnClickListener(this);

        addContactModel = new AddContactScreenModel(this);
        addContactPresenter = new AddContactScreenPresenter(addContactModel);
        addContactPresenter.addView(this);
        //get intent
        Intent intent = getIntent();
        if (intent != null) {
            String action = intent.getAction();
            if (ContactActivity.ACTION_ADD_CONTACT.equals(action)) {
                // only the user id will be reveived
                userID = intent.getStringExtra(ContactActivity.USER_ID);

            } else if (ContactActivity.ACTION_EDIT_CONTACT.equals(action)) {
                // user id and a contact will be received
                userID = intent.getStringExtra(ContactActivity.USER_ID);
                UserContact contact = intent.getParcelableExtra(CONTACT);
                contactName.setText(contact.getContactName());
                contactPhone.setText(contact.getContactPhone());

                if (contact.getContactNickName() != null)
                    contackNickName.setText(contact.getContactNickName());
                if (contact.getContactEmail() != null)
                    contactMail.setText(contact.getContactEmail());
                if (contact.getContactPhoto() != null)
                    Glide.with(this).load(contact.getContactPhoto()).into(contactPhoto);

            }
        }

    }
    @Override
    protected void onDestroy() {
        addContactPresenter.removeView();
        super.onDestroy();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            {
                if (requestCode == CAMERA_ACTION) {
                    if (getPickImageResultUri(this, data) != null) {
                        Uri picUri = getPickImageResultUri(this, data);
                        Glide.with(this).load(picUri).into(contactPhoto);
                        photUrl=picUri.toString();
                    }
                }
            }
        }
    }

    //endregion

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK);
        overridePendingTransition(R.anim.add_contact_enter, R.anim.add_contact_exit);
    }


    public static Intent getLauncherIntent(Context context, String userId) {
        Intent intent = new Intent(context, AddContactActivity.class);
        intent.putExtra(USER_ID, userId);
        return intent;
    }

    public static Intent getLauncherIntent(Context context, String userId, UserContact contact) {
        Intent intent = new Intent(context, AddContactActivity.class);
        intent.putExtra(USER_ID, userId);
        intent.putExtra(CONTACT, contact);
        return intent;
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();

        switch (id) {
            case R.id.ok_bt: {
                addContactPresenter.saveContactInformation(userID);
                break;
            }

            case R.id.canel_bt: {
                cancel.startAnimation(myAnim);
                onBackPressed();
                break;
            }

            case R.id.contact_choose_photo: {
                startActivityForResult(Utility.getPickImageChooserIntent(this), CAMERA_ACTION);
                break;
            }
        }
    }

    //region MVP methods
    @Override
    public String getContactName() {
        return contactName.getText().toString().trim();
    }

    @Override
    public String getContactEmail() {
        return contactMail.getText().toString().trim();
    }

    @Override
    public String getContactPhone() {
        return contactPhone.getText().toString().trim();
    }

    @Override
    public String getContactNickName() {
        return contackNickName.getText().toString().trim();
    }

    @Override
    public String getUserGender() {
        if(maleButton.isChecked())
            return "Male";
        return "Female";
    }

    @Override
    public String getPhotoUrl() {
        return photUrl;
    }

    @Override
    public void showContactInformation(UserContact contact) {
        contactName.setText(contact.getContactName());
        contactMail.setText(contact.getContactEmail());
        contackNickName.setText(contact.getContactNickName());
        contactPhone.setText(contact.getContactPhone());
        if(contact.getContactgender().equals("Male")) {
            maleButton.setChecked(true);
            femaleButton.setChecked(false);
        }
        else if(contact.getContactgender().equals("Female")) {
            femaleButton.setChecked(true);
            maleButton.setChecked(false);
        }
        }


    @Override
    public void showErrorMessage() {
        contactName.setError(getResources().getString(R.string.error_required));
        contactPhone.setError(getResources().getString(R.string.error_required));
    }

    @Override
    public void showContactAddedButtonAnimation() {
        saveUserdata.startAnimation(myAnim);
    }

    @Override
    public void showContacetMissingDataAnimation() {
        saveUserdata.startAnimation(myAnim2);
    }

    //endregion
}
