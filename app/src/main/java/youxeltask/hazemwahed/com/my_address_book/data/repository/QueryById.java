package youxeltask.hazemwahed.com.my_address_book.data.repository;

/**
 * Created by Hazem on 8/13/2017.
 * <p>
 * Create SQL query by ID statement.
 */

public class QueryById implements SQLStatement {

    private String id;
    private String idColName;
    private String table;
    private String order;

    public QueryById(String table, String id, String idColName) {
        this.id = id;
        this.idColName = idColName;
        this.table = table;
    }

    public void setOrder(String orderBy, String column) {
        order = " ORDER BY " + column + " " + orderBy;
    }

    @Override
    public String sqlStatement() {
        if (order == null)
            return "SELECT * FROM " + table + " WHERE " + idColName + " = '" + id + "'";
        else
            return "SELECT * FROM " + table + " WHERE " + idColName + " = '" + id + "'"+order;
    }
}
