package youxeltask.hazemwahed.com.my_address_book.data.repository;

/**
 * Created by Hazem on 8/6/2017.
 * <p>
 * This is a generic interface when implemented it should show
 * how to convert one object to another
 */

public interface Mapper<From, To> {
    /**
     * Implement this method to convert {@link From}  to a different object {@link To}
     *
     * @param from The object that will be converted.
     * @return A different object depends on how the method will be implemented.
     */
    To map(From from);
}
