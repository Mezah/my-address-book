package youxeltask.hazemwahed.com.my_address_book.mvp.welcomMVP;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.model.User;

/**
 * Created by Hazem on 10/7/2017.
 */

public interface WelcomScreenMVP {

    interface WelcomeModel {
        Observable<User> getUserData();
        boolean isUserSaved();
        void saveUserValue(User user);
        void updateUser(User user);
    }

    interface WelcomeView {

        void showUserName(String userName);

        void showUserPicture(String userPhoto);

        void goToUserContact();

        void setUser(User user);

    }

    interface WelcomePresenter {

        void addView(WelcomeView welcomeView);
        void removeView();
        boolean isViewAdded();
        void showUserInformation();
        void updateUser(User user);
        void saveUser(User user);
    }
}
