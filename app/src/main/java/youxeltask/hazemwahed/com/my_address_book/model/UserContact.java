package youxeltask.hazemwahed.com.my_address_book.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hazem on 10/6/2017.
 */

public class UserContact implements Parcelable{

    private String contactName;
    private String contactPhone;
    private String contactEmail;
    private String contactNickName;
    private String contactPhoto;
    private String Contactgender;
    private String userId;

    public UserContact(){

    }

    protected UserContact(Parcel in) {
        contactName = in.readString();
        contactPhone = in.readString();
        contactEmail = in.readString();
        contactNickName = in.readString();
        contactPhoto = in.readString();
        Contactgender = in.readString();
        userId = in.readString();
    }

    public static final Creator<UserContact> CREATOR = new Creator<UserContact>() {
        @Override
        public UserContact createFromParcel(Parcel in) {
            return new UserContact(in);
        }

        @Override
        public UserContact[] newArray(int size) {
            return new UserContact[size];
        }
    };

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactNickName() {
        return contactNickName;
    }

    public void setContactNickName(String contactNickName) {
        this.contactNickName = contactNickName;
    }

    public String getContactPhoto() {
        return contactPhoto;
    }

    public void setContactPhoto(String contactPhoto) {
        this.contactPhoto = contactPhoto;
    }

    public String getContactgender() {
        return Contactgender;
    }

    public void setContactgender(String contactgender) {
        Contactgender = contactgender;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contactName);
        dest.writeString(contactPhone);
        dest.writeString(contactEmail);
        dest.writeString(contactNickName);
        dest.writeString(contactPhoto);
        dest.writeString(Contactgender);
        dest.writeString(userId);
    }
}
