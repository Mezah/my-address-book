package youxeltask.hazemwahed.com.my_address_book.data.repository;

/**
 * Created by Hazem on 10/6/2017.
 *
 * This is an interface that return SQL statement.
 */

public interface SQLStatement {
    String sqlStatement();
}
