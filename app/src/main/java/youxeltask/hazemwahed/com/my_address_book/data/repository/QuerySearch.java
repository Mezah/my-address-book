package youxeltask.hazemwahed.com.my_address_book.data.repository;

/**
 * Created by Hazem on 10/7/2017.
 */

public class QuerySearch implements SQLStatement {

    private String  search;

    public void setSearch(String search) {
        this.search = search;
    }

    @Override
    public String sqlStatement() {
        return "SELECT * FROM "+ DatabaseContract.ContactTable.TABLE_NAME;
    }
}
