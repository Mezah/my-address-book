package youxeltask.hazemwahed.com.my_address_book.mvp.addcontactMVP;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;

/**
 * Created by Hazem on 10/7/2017.
 */

public interface AddContactMVP {

    interface AddContactModel{

        void saveContactInformation(UserContact contact);

        Observable<UserContact> getContactFromDatabase(String contactName);

    }

    interface AddContactView{

        String getContactName();

        String getContactEmail();

        String getContactPhone();

        String getContactNickName();

        String getUserGender();

        String getPhotoUrl();

        void showContactInformation(UserContact contact);

        void showErrorMessage();

        void showContactAddedButtonAnimation();

        void showContacetMissingDataAnimation();
    }

    interface AddContactPresenter {

        void addView(AddContactView addContactView);
        void removeView();
        boolean isViewAdded();
        void getContactInformation(String userId, String contactName, String contactPhone) ;

        void saveContactInformation(String userID);
    }
}
