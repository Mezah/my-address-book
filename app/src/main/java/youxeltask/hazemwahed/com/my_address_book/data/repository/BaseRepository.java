package youxeltask.hazemwahed.com.my_address_book.data.repository;


import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Hazem on 10/6/2017.
 *
 * This Interface represent the CRUD operations that can be performed using any Model class.
 * This interface delegate the implementation of the methods to the implementer without putting any
 * conditions on how or what kind of tool to user in order to persists application data.
 */

public interface BaseRepository<T> {

    void add(T item);

    void add(List<T> items);

    boolean update(T item);

    boolean remove(T item);

    Observable<T> singleItemQuery(SQLStatement specification);

    Observable<List<T>> query(SQLStatement query);

}
