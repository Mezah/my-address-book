package youxeltask.hazemwahed.com.my_address_book.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import youxeltask.hazemwahed.com.my_address_book.R;
import youxeltask.hazemwahed.com.my_address_book.model.User;
import youxeltask.hazemwahed.com.my_address_book.mvp.welcomMVP.WelcomScreenMVP;
import youxeltask.hazemwahed.com.my_address_book.mvp.welcomMVP.WelcomeScreenModel;
import youxeltask.hazemwahed.com.my_address_book.mvp.welcomMVP.WelcomeScreenPresenter;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;

import static youxeltask.hazemwahed.com.my_address_book.activities.WelcomeActivity.CAMERA_ACTION;
import static youxeltask.hazemwahed.com.my_address_book.utility.Utility.getPickImageResultUri;

/**
 * Created by Hazem on 10/6/2017.
 */

public class WelcomeFragment extends Fragment implements WelcomScreenMVP.WelcomeView, View.OnClickListener {

    public static final String FRAGMENT_TAG = "FRAG_MAIN";
    private ImageView userImageView, choosePicture;
    private EditText userNameET;
    private WelcomScreenMVP.WelcomePresenter welcomePresenter;
    private WelcomScreenMVP.WelcomeModel welcomeModel;
    private ContactViewer contactViewer;
    private ImageButton goToContact;
    private User mUser;

    //region fragment lifecycle
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        contactViewer = (ContactViewer) getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        welcomeModel = new WelcomeScreenModel(getActivity());
        welcomePresenter = new WelcomeScreenPresenter(getActivity(), welcomeModel);
        welcomePresenter.addView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        userImageView = view.findViewById(R.id.user_photo);
        choosePicture = view.findViewById(R.id.choose_photo);
        userNameET = view.findViewById(R.id.user_name_ET);
        goToContact = view.findViewById(R.id.go_to_contact);

        goToContact.setOnClickListener(this);
        choosePicture.setOnClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!welcomePresenter.isViewAdded())
            welcomePresenter.addView(this);
        //get user information from database
        welcomePresenter.showUserInformation();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            {
                if (requestCode == CAMERA_ACTION) {
                    if (getPickImageResultUri(getActivity(), data) != null) {
                        Uri picUri = getPickImageResultUri(getActivity(), data);
                        showUserPicture(picUri.toString());
                        welcomePresenter.updateUser(mUser);
                    }
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        welcomePresenter.removeView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        contactViewer = null;
    }

    //endregion

    //region view click listener

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        final Animation myAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.button_scale);
        switch (id) {
            case R.id.choose_photo: {
                startActivityForResult(Utility.getPickImageChooserIntent(getActivity()), CAMERA_ACTION);
                break;
            }
            case R.id.go_to_contact: {
                goToContact.startAnimation(myAnim);
                goToUserContact();
                break;
            }
        }
    }

    //endregion

    //region MVP methods

    @Override
    public void showUserName(String userName) {
        userNameET.setText(userName);
    }

    @Override
    public void showUserPicture(String userPhoto) {
        Glide.with(getActivity()).load(userPhoto).into(userImageView);
        mUser.setPhotoUrl(userPhoto);
    }


    @Override
    public void goToUserContact() {
        if (!userNameET.getText().toString().trim().isEmpty()) {
            mUser.setUserName(userNameET.getText().toString().trim());
            // update user data
            welcomePresenter.updateUser(mUser);
            contactViewer.viewContact(mUser.getUserId());
        } else
            Toast.makeText(getActivity(), "Please Enter Your Name", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setUser(User user) {
        mUser = user;
    }

    //endregion

    //region interfaces

    /**
     * This interface will be implemented by the activity that will host this fragment.
     * It will handle launching the contact view.
     */
    public interface ContactViewer {
        void viewContact(String userID);
    }

    //endregion
}
