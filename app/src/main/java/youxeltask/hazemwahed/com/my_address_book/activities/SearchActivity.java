package youxeltask.hazemwahed.com.my_address_book.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import jp.wasabeef.recyclerview.animators.LandingAnimator;
import youxeltask.hazemwahed.com.my_address_book.R;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;

import static youxeltask.hazemwahed.com.my_address_book.activities.ContactActivity.ACTION_EDIT_CONTACT;
import static youxeltask.hazemwahed.com.my_address_book.activities.ContactActivity.USER_ID;
import static youxeltask.hazemwahed.com.my_address_book.fragments.ContactFragment.CONTACT_LIST;


/**
 * Created by Hazem on 10/1/2017.
 */

public class SearchActivity extends AppCompatActivity {

    public static final int ANIMTAION_TIME=200;
    private EditText searchEditText;
    private RecyclerView searchResultRecylcer;
    private ContactAdapter contactAdapter;
    private ImageView backToPreviousScreen;
    private Disposable disposable;
    private ArrayList<UserContact> contacts;
    private String userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setTitle(R.string.search);
        searchEditText = findViewById(R.id.activity_search_editText);
        searchResultRecylcer = findViewById(R.id.activity_search_result_recycler);
        searchResultRecylcer.setItemAnimator(new LandingAnimator());
        searchResultRecylcer.getItemAnimator().setRemoveDuration(ANIMTAION_TIME);
        searchResultRecylcer.getItemAnimator().setChangeDuration(ANIMTAION_TIME);
        searchResultRecylcer.getItemAnimator().setMoveDuration(ANIMTAION_TIME);
        searchResultRecylcer.getItemAnimator().setAddDuration(ANIMTAION_TIME);

        backToPreviousScreen = findViewById(R.id.toolbar_search_back);
        backToPreviousScreen.setOnClickListener(view -> onBackPressed());
        prepareAdapter();
        prepareRecycer();

        Intent intent = getIntent();
        if (intent != null) {
            contacts = intent.getParcelableArrayListExtra(CONTACT_LIST);
            userId=intent.getParcelableExtra(USER_ID);
        }
        disposable = RxTextView.textChangeEvents(searchEditText)
                .doOnSubscribe(disposable1 -> hideSoftKeyboard(searchEditText))
                .filter(text -> isNotNullOrEmpty(text.text().toString()))
                .map(textChange -> textChange.text().toString().trim())
                .filter(searchText -> contacts!=null && !contacts.isEmpty()) // check if the list is not empty or null
                .switchMap(new Function<String, ObservableSource<List<UserContact>>>() {
                    @Override
                    public ObservableSource<List<UserContact>> apply(@NonNull String searchText) throws Exception {
                        return Observable.fromIterable(contacts)
                                .filter(item -> {
                                    String name=item.getContactName().toLowerCase();
                                    String phone=item.getContactPhone();
                                    boolean nameFound=name.contains(searchText);
                                    boolean phoneFound=phone.contains(searchText);
                                    Log.d("Search_items: ","name: "+name+" has "+searchText);
                                    Log.d("Search_items: ","phone: "+phone+" has "+searchText);
                                    return nameFound || phoneFound;
                                }).toList().toObservable();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(getBookListObserver());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.anim_search_out);
    }

    public void showEditContact(UserContact contact, String userID) {
        Intent intent = AddContactActivity.getLauncherIntent(this, userID, contact);
        intent.setAction(ACTION_EDIT_CONTACT);
        startActivityForResult(intent, 20);
        overridePendingTransition(R.anim.add_contact_enter, R.anim.add_contact_exit);
    }

    private void prepareAdapter() {
        contactAdapter = new ContactAdapter();
        contactAdapter.setContactList(new ArrayList<>());
    }

    public void prepareRecycer() {
        searchResultRecylcer.setAdapter(contactAdapter);
        searchResultRecylcer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
    }

    private boolean isNotNullOrEmpty(String text) {
        if (text.isEmpty()) {
            contactAdapter.setContactList(new ArrayList<>());
        }
        return !isNullOrEmpty(text);
    }

    private boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private DisposableObserver<List<UserContact>> getBookListObserver() {
        return new DisposableObserver<List<UserContact>>() {
            @Override
            public void onNext(@NonNull List<UserContact> bookList) {
                // check if the thread is the main thread
                contactAdapter.setContactList(bookList);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.d("SearchActivity", e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {
                Log.d("SearchActivity", "User stop writing");
            }
        };
    }

    private final class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {

        private List<UserContact> contactList;


        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item_list_search, parent, false);
            ContactViewHolder viewHolder = new ContactViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ContactViewHolder holder, int position) {
            UserContact contact = contactList.get(position);
            holder.setContact(contact);
        }

        @Override
        public int getItemCount() {
            return contactList.size();
        }

        public void setContactList(List<UserContact> contactList) {
            if (this.contactList != null)
                if (!this.contactList.isEmpty())
                    this.contactList.clear();
            this.contactList = contactList;
            notifyDataSetChanged();
        }
    }

    private final class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView photo;
        private final TextView name, phone;
        private UserContact userContact;
        private LinearLayout container;


        public ContactViewHolder(View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.list_contact_photo);
            name = itemView.findViewById(R.id.list_contact_name);
            phone = itemView.findViewById(R.id.list_contact_phone);
            container = itemView.findViewById(R.id.list_container);
            container.setOnClickListener(this);
        }

        public void setContact(UserContact contact) {
            this.userContact = contact;
            name.setText(contact.getContactName());
            phone.setText(contact.getContactPhone());
            if (contact.getContactPhoto() != null)
                Glide.with(SearchActivity.this).load(contact.getContactPhoto()).into(photo);
        }

        @Override
        public void onClick(View v) {
            final int id = v.getId();
            switch (id) {
                case R.id.list_container: {
                    showEditContact(userContact, userId);
                    break;
                }
            }
        }
    }
    //endregion
}
