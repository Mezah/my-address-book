package youxeltask.hazemwahed.com.my_address_book.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.List;

import youxeltask.hazemwahed.com.my_address_book.R;
import youxeltask.hazemwahed.com.my_address_book.fragments.ContactFragment;
import youxeltask.hazemwahed.com.my_address_book.model.UserContact;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;

/**
 * Created by Hazem on 10/7/2017.
 */

public class ContactActivity extends AppCompatActivity implements ContactFragment.AddContactViewer {

    public static final String USER_ID = "user_id";
    public static final String CONTACT = "USER_CONTACT";
    public static final String ACTION_ADD_CONTACT = "contact.action.add_new_contact";
    public static final String ACTION_EDIT_CONTACT = "contact.action.edit_contact";

    private static final int CONTAINER = R.id.contact_container;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avtivity_contact);
        getSupportActionBar().setTitle(R.string.contact);

        if (getIntent() != null) {
            Intent intent = getIntent();
            String userId = intent.getStringExtra(USER_ID);

            Bundle bundle = new Bundle();
            bundle.putString(USER_ID, userId);
            ContactFragment contactFragment = ContactFragment.getContactFragment(bundle);
            Utility.addFragment(this, contactFragment, CONTAINER, ContactFragment.FRAGMENT_TAG, false, false);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ContactFragment contactFragment = (ContactFragment) getSupportFragmentManager().findFragmentByTag(ContactFragment.FRAGMENT_TAG);
        contactFragment.updateContactList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menus, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_dummy_data: {
                List<UserContact> contacts=Utility.createUserContactDummyList();
                ContactFragment contactFragment = (ContactFragment) getSupportFragmentManager().findFragmentByTag(ContactFragment.FRAGMENT_TAG);
                contactFragment.createDummyData(contacts);
                break;
            }
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.entr_from_left, R.anim.exit_to_right);
    }

    /**
     * Create the intent that is required to launch this activity with all the required data.
     *
     * @param context The Context that will launch the activity.
     * @param userID  The data that will be used to launch the activity.
     * @return An intent that will start this activity.
     */
    public static Intent getLaunchIntent(Context context, String userID) {
        Intent intent = new Intent(context, ContactActivity.class);
        intent.putExtra(USER_ID, userID);
        return intent;
    }

    @Override
    public void showNewAddContactScreen(String userId) {
        Intent intent = AddContactActivity.getLauncherIntent(this, userId);
        intent.setAction(ACTION_ADD_CONTACT);
        startActivityForResult(intent, 20);
        overridePendingTransition(R.anim.add_contact_enter, R.anim.add_contact_exit);
    }

    @Override
    public void showEditContact(UserContact contact, String userID, int contactPosition) {
        Intent intent = AddContactActivity.getLauncherIntent(this, userID, contact);
        intent.setAction(ACTION_EDIT_CONTACT);
        startActivityForResult(intent, 20);
        overridePendingTransition(R.anim.add_contact_enter, R.anim.add_contact_exit);
    }
}
