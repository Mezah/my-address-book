package youxeltask.hazemwahed.com.my_address_book.data.repository;

import android.provider.BaseColumns;

/**
 * Created by Hazem on 10/6/2017.
 */

public class DatabaseContract {


    public static final class UserTable implements BaseColumns {

        public static final String TABLE_NAME = "user_table";

        public static final String USER_ID_COL = "user_id";

        public static final String USER_NAME = "user_name";

        public static final String USER_PHOTO = "user_photo";

        public static final String CREATE_STATEMENT = " CREATE TABLE " + TABLE_NAME + "( " +
                UserTable._ID + " INTEGER PRIMARY KEY, " +  // only one row for one user
                USER_ID_COL + " TEXT UNIQUE ON CONFLICT REPLACE," +
                USER_NAME + " TEXT," +
                USER_PHOTO + " TEXT);";

    }

    public static final class ContactTable implements BaseColumns {

        public static final String TABLE_NAME = "contacts_table";

        public static final String CONTACT_NAME_COL = "contact_name";

        public static final String CONTACT_PHONE_NUMBER = "contact_phone";

        public static final String CONTACT_NICK_NAME = "contact_nick_name";

        public static final String CONTACT_EMAIL = "contact_email";

        public static final String CONTACT_PHOTO = "contact_photo";

        public static final String CONTACT_GENDER = "contact_gender";

        public static final String CONTACT_USER_ID="contact_user_id";

        public static final String CREATE_STATEMENT = " CREATE TABLE " + TABLE_NAME + "( " +
                ContactTable._ID + " INTEGER PRIMARY KEY, " +
                CONTACT_NAME_COL + " TEXT NOT NULL UNIQUE ON CONFLICT REPLACE," +
                CONTACT_PHONE_NUMBER + " TEXT NOT NULL," +
                CONTACT_NICK_NAME + " TEXT," +
                CONTACT_EMAIL + " TEXT," +
                CONTACT_PHOTO + " TEXT," +
                CONTACT_GENDER + " TEXT," +
                CONTACT_USER_ID + " TEXT);";
    }
}
