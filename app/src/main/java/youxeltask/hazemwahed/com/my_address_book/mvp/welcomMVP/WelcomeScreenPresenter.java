package youxeltask.hazemwahed.com.my_address_book.mvp.welcomMVP;

import android.content.Context;
import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import youxeltask.hazemwahed.com.my_address_book.model.User;
import youxeltask.hazemwahed.com.my_address_book.utility.Utility;

/**
 * Created by Hazem on 10/7/2017.
 */

public class WelcomeScreenPresenter implements WelcomScreenMVP.WelcomePresenter {

    private static final String TAG = "WelcomScreenPresenter";

    private WelcomScreenMVP.WelcomeModel welcomeModel;
    private WelcomScreenMVP.WelcomeView welcomeView;
    private Context context;
    private CompositeDisposable disposable;
    private User mUser;

    public WelcomeScreenPresenter(Context context, WelcomScreenMVP.WelcomeModel welcomeModel) {
        this.welcomeModel = welcomeModel;
        this.context = context;
        disposable = new CompositeDisposable();
    }

    @Override
    public void addView(WelcomScreenMVP.WelcomeView welcomeView) {
        this.welcomeView = welcomeView;
    }

    @Override
    public void removeView() {
        if (disposable != null)
            disposable.dispose();
        welcomeView = null;
    }

    @Override
    public boolean isViewAdded() {
        return welcomeView != null;
    }

    @Override
    public void showUserInformation() {

        welcomeModel.getUserData()
                .subscribe(new Observer<User>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(@NonNull User user) {
                        welcomeView.setUser(user);
                        if (!user.getUserName().equals(Utility.NO_DATA)) {
                            welcomeView.showUserName(user.getUserName());
                        }
                        if (!user.getPhotoUrl().equals(Utility.NO_DATA)) {
                            welcomeView.showUserPicture(user.getPhotoUrl());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d(TAG, "onError: " + e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: stream completed");
                        disposable.dispose();
                    }
                });
    }

    @Override
    public void updateUser(User user) {
        welcomeModel.saveUserValue(user);
    }

    @Override
    public void saveUser(User user) {
        welcomeModel.saveUserValue(user);
    }

}
