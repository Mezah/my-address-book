package youxeltask.hazemwahed.com.my_address_book.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import youxeltask.hazemwahed.com.my_address_book.R;
import youxeltask.hazemwahed.com.my_address_book.fragments.WelcomeFragment;

import static youxeltask.hazemwahed.com.my_address_book.utility.Utility.addFragment;

public class WelcomeActivity extends AppCompatActivity implements WelcomeFragment.ContactViewer {

    private static final int FRAGMENT_CONTAINER = R.id.container;
    public static final int CAMERA_ACTION = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        getSupportActionBar().setTitle(R.string.welcome);
        WelcomeFragment fragment = new WelcomeFragment();
        addFragment(this, fragment, FRAGMENT_CONTAINER, WelcomeFragment.FRAGMENT_TAG, false, false);
    }

    @Override
    public void viewContact(String userID) {
        Intent intent = ContactActivity.getLaunchIntent(this, userID);
        startActivity(intent);
        overridePendingTransition(R.anim.entre_from_right, R.anim.exit_to_left);

    }
}
