package youxeltask.hazemwahed.com.my_address_book.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;

import java.util.List;

import io.reactivex.Observable;
import youxeltask.hazemwahed.com.my_address_book.utility.schedulers.SchedulerProvider;

/**
 * Created by Hazem on 10/6/2017.
 * <p>
 * This class is used to manage database operation. That helps to isolate the data layer and make it
 * independent on the type of the database used.
 */

public class DatabaseManager {

    private static final String TAG = "DatabaseManager";

    public static final String USER_SAVED="user_state";

    private static DatabaseManager dbManager;
    private DBHelper helper;
    private SqlBrite sqlBrite;
    private static final String SHARED_NAME = "MY_SHARED";
    private BriteDatabase briteDatabase;
    private SharedPreferences sp;

    private DatabaseManager(Context context, SchedulerProvider schedulerProvider) {
        helper = createOpenHelper(context);
        sqlBrite = new SqlBrite.Builder().build();
        briteDatabase = sqlBrite.wrapDatabaseHelper(helper, schedulerProvider.io());
        sp = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Create an instance of {@link android.database.sqlite.SQLiteOpenHelper} that will create the local database
     *
     * @param context Application Context.
     * @return Instance of {@link android.database.sqlite.SQLiteOpenHelper}.
     */
    private DBHelper createOpenHelper(Context context) {
        if (helper == null)
            helper = new DBHelper(context);
        return helper;
    }

    /**
     * Create an instance of the database manager and make sure only one instance will be created.
     * Double check local implementation to make sure that the singleton will not affected by multi-thread opearation
     *
     * @param context           Application context.
     * @param schedulerProvider Class that provides different type of threads.
     * @return A new instance if this the first call or an old instance if the method was called before.
     */
    public static DatabaseManager getDatabaseManager(Context context, SchedulerProvider schedulerProvider) {
        if (dbManager == null) {
            synchronized (DatabaseManager.class) {    // singleton with double check lock implementation
                if (dbManager == null) {
                    dbManager = new DatabaseManager(context, schedulerProvider);
                }
            }
        }
        return dbManager;
    }

    /**
     * Insert Single set of values into database table.
     * The method in {@link BriteDatabase} returns the row number where the data inserted or -1 if
     * that data can't be inserted.
     * A check is performed on the number to make sure the data is inserted.
     *
     * @param tableName      Table name where the data will be saved.
     * @param insertedValues The value that will be saved in the table.
     * @return True if the data inserted successfully, false otherwise.
     */
    public boolean insert(String tableName, ContentValues insertedValues) {
        long id = briteDatabase.insert(tableName, insertedValues);
        Log.d(TAG, "insert: id= " + id);
        return id > 0;
    }

    /**
     * Insert an array of values into database table.
     * The method in {@link BriteDatabase} returns the row numbers where the data inserted or -1 if
     * that data can't be inserted.
     * A check is performed on the number to make sure the data is inserted.
     *
     * @param tableName      Table name where the data will be saved.
     * @param insertedValues The values that will be saved in the table.
     * @return True if the data inserted successfully, false otherwise.
     */
    public boolean insert(String tableName, List<ContentValues> insertedValues) {
        final int entries = insertedValues.size();
        int cnt = 0;
        BriteDatabase.Transaction transaction = briteDatabase.newTransaction();
        try {
            for (ContentValues value : insertedValues) {
                long id = briteDatabase.insert(tableName, value);
                if (id != -1)
                    cnt++;
            }
            transaction.markSuccessful();
        } finally {
            transaction.end();
        }
        return cnt == entries;
    }

    /**
     * Update a value already existed in a database table.
     * The method in {@link BriteDatabase} returns the row numbers where the data is updated or -1 if
     * that data can't be updated.
     *
     * @param tableName     Table name where the data will be updated.
     * @param updatedValues The new value.
     * @param where         he Where clause to be applied.
     * @param whereClause   The value names in the where clause that will be replaced with the new values.
     * @return True if the update success false otherwise.
     */
    public boolean update(String tableName, ContentValues updatedValues, String where, String[] whereClause) {
        return briteDatabase.update(tableName, updatedValues, where, whereClause) > 0;
    }

    /**
     * Delete a value already existed in a database table.
     * The method in {@link BriteDatabase} returns the row numbers where the data is deleted or -1 if
     * that data can't be updated.
     *
     * @param tableName   Table name where the data will be deleted.
     * @param where       he Where clause to be applied.
     * @param whereClause The value names in the where clause that will be replaced with the new values.
     * @return True if the item is deleted success false otherwise.
     */
    public boolean delete(String tableName, String where, String... whereClause) {
        return briteDatabase.delete(tableName, where, whereClause) > 0;
    }

    /**
     * Query a database table for certain value or values.
     *
     * @param tableName Table name where the query will be performed .
     * @param sql       The query statement that will be performed.
     * @return An {@link Observable} of {@link SqlBrite} {@link com.squareup.sqlbrite2.SqlBrite.Query} that contains a cursor
     * with all the data.
     */
    public Observable<SqlBrite.Query> query(String tableName, String sql) {
        return briteDatabase.createQuery(tableName, sql);
    }

    public boolean getBoolean(String key){
        return sp.getBoolean(key,false);
    }
    /**
     * close that database after finishing all the required operations.
     */
    public void clear() {
        helper.close();
        briteDatabase.close();
    }
}
