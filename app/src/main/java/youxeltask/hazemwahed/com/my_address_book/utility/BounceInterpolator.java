package youxeltask.hazemwahed.com.my_address_book.utility;

import android.view.animation.Interpolator;

/**
 * Created by Hazem on 10/9/2017.
 * <p>
 * Add bouncing effect to button
 * Credit goes to
 * See <a href="http://evgenii.com/blog/spring-button-animation-on-android/">Button Animation</a>
 */

public class BounceInterpolator implements Interpolator {
    private double mAmplitude = 1;
    private double mFrequency = 10;

    public BounceInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}
